// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => [
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0);     //red ff0000
            const greenHex = converter.rgbToHex(0,255,0);   //green 00ff00
            const blueHex = converter.rgbToHex(0,0,255);    //blue 0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        })
    ]);

    describe("Hex to RGB conversion", function() {
        it("converts the basic colors", function() {
            const redRgb = converter.hexToRgb("ff0000");
            const greenRgb = converter.hexToRgb("00ff00");
            const blueRgb = converter.hexToRgb("0000ff");

            expect(redRgb).to.deep.equal([255, 0, 0]);
            expect(greenRgb).to.deep.equal([0, 255, 0]);
            expect(blueRgb).to.deep.equal([0, 0, 255]);

        });
    });
});